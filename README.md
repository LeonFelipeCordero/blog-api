# blog-api

### Setup
Requirements
- JDK 11
- Docker
- docker-compose
- K8S cluster (check kind to run local)
- MongoDB instance

### Build
````
./gradlew clean build
````

### Run Tests
```
./gradlew check
```

### Run App
#### gradle
make sure you have a mongodb instance running in port the default port
```
SPRING_PROFILES_ACTIVE=dev ./gradlew bootRun
```
#### docker
make sure you have a mongodb instance running in port the default port
```
docker pull registry.gitlab.com/leonfelipecordero/blog-api
Or build the image by yourself
docker build -t registry.gitlab.com/leonfelipecordero/blog-api -f ci/Dockerfile .
docker run -p 8080 -e SPRING_PROFILES_ACTIVE=dev registry.gitlab.com/leonfelipecordero/blog-api
```
#### docker-compose
```
docker-compose -f ci/docker-compose.yml up
```
#### k8s (kind)
```
./env/kind/strat-kind.sh 
```
It will start a kind cluster in your local machine, next step is to deploy a mongodb instance
I will recommend use helm: https://helm.sh/docs/intro/install/
```
helm repo add bitnami https://charts.bitnami.com/bitnami
helm install mongodb --set usePassword=false bitnami/mongodb
```
Now only remains deploy the application
```
kubectl apply -f ci/k8s/deployment.yml
```
