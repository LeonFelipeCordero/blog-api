#!/bin/sh

echo "sleeping 20 seconds to wait for mongodb to start first"
sleep 20

echo "starting docker container"
java -jar app.jar
