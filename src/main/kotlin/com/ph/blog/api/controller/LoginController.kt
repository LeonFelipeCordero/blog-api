package com.ph.blog.api.controller

import com.ph.blog.api.model.basic.BasicProfile
import com.ph.blog.api.model.request.LoginRequest
import com.ph.blog.api.service.LoginService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping(value = ["login"])
class LoginController(private val loginService: LoginService) {

    @PutMapping
    fun login(@Valid @RequestBody loginRequest: LoginRequest): ResponseEntity<BasicProfile> =
            ResponseEntity(loginService.login(loginRequest), HttpStatus.OK)
}
