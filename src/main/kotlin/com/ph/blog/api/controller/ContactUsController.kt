package com.ph.blog.api.controller

import com.ph.blog.api.model.request.ContactUsRequest
import com.ph.blog.api.model.response.SuccessResponseEntity
import com.ph.blog.api.model.response.SuccessResponseEntity.Companion.accepted
import com.ph.blog.api.service.ContactUsService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@CrossOrigin
@RestController
class ContactUsController(private val contactUsService: ContactUsService) {

    @PostMapping(value = ["contact-us"])
    fun contactUs(@Valid @RequestBody contactUsRequest: ContactUsRequest):SuccessResponseEntity {
        contactUsService.sendEmail(contactUsRequest)
        return accepted("Email Sent")
    }
}
