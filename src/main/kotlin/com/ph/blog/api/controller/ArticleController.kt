package com.ph.blog.api.controller

import com.ph.blog.api.model.Article
import com.ph.blog.api.model.request.ArticleRequest
import com.ph.blog.api.model.request.CommentRequest
import com.ph.blog.api.model.response.SuccessResponseEntity
import com.ph.blog.api.model.response.SuccessResponseEntity.Companion.accepted
import com.ph.blog.api.service.ArticleService
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.time.LocalDate
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping(value = ["article"])
class ArticleController(private val articleService: ArticleService) {

    @GetMapping(value = ["/{articleId}"])
    fun getArticleById(@PathVariable("articleId") articleId: String): ResponseEntity<Article> =
            ResponseEntity(articleService.findArticleById(articleId), HttpStatus.OK)

    @GetMapping
    fun getArticle(@RequestParam(name = "title", required = false) title: String?,
                   @RequestParam(name = "author", required = false) author: String?,
                   @RequestParam(name = "category", required = false) category: String?,
                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                   @RequestParam(name = "date", required = false) date: LocalDate?): ResponseEntity<List<Article>> =
            ResponseEntity(articleService.findArticle(title, author, category, date), HttpStatus.OK)

    @GetMapping(value = ["/top"])
    fun getTopArticlesByDateAfter(): ResponseEntity<List<Article>> =
            ResponseEntity(articleService.findTopByPostDateAfter(LocalDate.now().minusDays(5)), HttpStatus.OK)

    @PostMapping
    fun save(@Valid @RequestBody articleRequest: ArticleRequest): ResponseEntity<Article> =
            ResponseEntity(articleService.save(articleRequest), HttpStatus.ACCEPTED)

    @PutMapping("/{articleId}/comment")
    fun addComment(@Valid @RequestBody commentRequest: CommentRequest,
                   @PathVariable("articleId") articleId: String): SuccessResponseEntity {
        articleService.addComment(articleId, commentRequest)
        return accepted("Comment Saved")
    }

    @PutMapping(value = ["/{articleId}"])
    fun update(@Valid @RequestBody articleRequest: ArticleRequest,
               @PathVariable("articleId") articleId: String): ResponseEntity<Article> =
            ResponseEntity(articleService.save(articleRequest, articleId), HttpStatus.ACCEPTED)

    @DeleteMapping(value = ["/{articleId}"])
    fun delete(@PathVariable("articleId") articleId: String): SuccessResponseEntity {
        articleService.deleteArticle(articleId)
        return accepted("Article deleted")
    }

}
