package com.ph.blog.api.controller

import com.ph.blog.api.model.Profile
import com.ph.blog.api.model.basic.BasicProfile
import com.ph.blog.api.model.request.ProfileRequest
import com.ph.blog.api.model.request.UpdateProfileRequest
import com.ph.blog.api.model.response.SuccessResponseEntity
import com.ph.blog.api.model.response.SuccessResponseEntity.Companion.accepted
import com.ph.blog.api.service.ProfileService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping(value = ["profile"])
class ProfileController(private val profileService: ProfileService) {

    @GetMapping(value = ["/{id}"])
    fun getProfileByEmail(@PathVariable(value = "id", required = true) id: String): ResponseEntity<Profile> =
            ResponseEntity(profileService.findProfileById(id), HttpStatus.OK)

    @PostMapping
    fun save(@Valid @RequestBody profileRequest: ProfileRequest): ResponseEntity<BasicProfile> =
            ResponseEntity(profileService.save(profileRequest), HttpStatus.ACCEPTED)

    @DeleteMapping(value = ["/{id}"])
    fun delete(@PathVariable(value = "id", required = true) id: String): SuccessResponseEntity {
        profileService.delete(id)
        return accepted("User Deleted")
    }

    @PutMapping(value = ["/{id}/author"])
    fun becomeAuthor(@PathVariable(value = "id", required = true) id: String): ResponseEntity<BasicProfile> =
            ResponseEntity(profileService.updateToAuthor(id), HttpStatus.OK)

    @PutMapping(value = ["/{id}"])
    fun update(@Valid @RequestBody profileRequest: UpdateProfileRequest,
               @PathVariable(value = "id") id: String): ResponseEntity<BasicProfile> =
            ResponseEntity(profileService.save(profileRequest, id), HttpStatus.ACCEPTED)

}
