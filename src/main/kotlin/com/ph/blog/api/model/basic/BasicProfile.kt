package com.ph.blog.api.model.basic

data class BasicProfile(var id: String? = null,
                        var firstName: String? = null,
                        var lastName: String? = null,
                        val userName: String? = null,
                        val email: String? = null,
                        val isAuthor: Boolean? = null)
