package com.ph.blog.api.model.request

import com.ph.blog.api.model.Article
import java.time.LocalDate
import javax.validation.constraints.NotEmpty

data class ArticleRequest(@field:NotEmpty(message = "title can't be empty")
                          val title: String? = null,
                          @field:NotEmpty(message = "content can't be empty")
                          val blogHtml: String? = null,
                          @field:NotEmpty(message = "description can't be empty")
                          val shortDescription: String? = null,
                          val author: String? = null,
                          @field:NotEmpty(message = "category can't be empty")
                          val category: String? = null) {

    fun toArticle() =
            Article(blogHtml = this.blogHtml,
                    title = this.title,
                    shortDescription = this.shortDescription,
                    postDate = LocalDate.now(),
                    category = this.category,
                    author = this.author,
                    comments = mutableListOf())
}
