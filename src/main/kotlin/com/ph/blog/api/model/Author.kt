package com.ph.blog.api.model

data class Author(var profile_id: String,
                  var website: String? = null,
                  var company: String? = null)
