package com.ph.blog.api.model.response

import org.springframework.http.HttpStatus
import java.time.LocalDateTime

data class SuccessResponse(val status: HttpStatus, val message: String, val timestamp: LocalDateTime) {

    constructor(status: HttpStatus, message: String) :
            this(status, message, LocalDateTime.now())
}
