package com.ph.blog.api.model

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class Comment(var profile: String? = null,
                   @field:NotNull(message = "comment can't not be null ot empty")
                   @field:NotEmpty(message = "comment can't not be null ot empty")
                   var content: String? = null)
