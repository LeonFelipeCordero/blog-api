package com.ph.blog.api.model.request

import com.ph.blog.api.model.Comment
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class CommentRequest(@field:NotNull(message = "comment can't not be null ot empty")
                          val comment: Comment)
