package com.ph.blog.api.model.request

import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class ContactUsRequest(@field:NotNull(message = "name can't be empty or null")
                            @field:NotEmpty(message = "name can't be empty or null")
                            val name: String,
                            @field:NotNull(message = "email can't be empty or null")
                            @field:NotEmpty(message = "email can't be empty or null")
                            val email: String,
                            @field:NotNull(message = "content can't be empty or null")
                            @field:NotEmpty(message = "content can't be empty or null")
                            val content: String)
