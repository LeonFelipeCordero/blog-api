package com.ph.blog.api.model.request

import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

class LoginRequest(@field:Email(message = "wrong email")
                   @field:NotNull(message = "email can't be null or empty")
                   @field:NotEmpty(message = "email can't be null or empty")
                   var email: String,
                   @field:NotNull(message = "Password can't be null or empty")
                   @field:NotEmpty(message = "Password can't be null or empty")
                   var password: String)
