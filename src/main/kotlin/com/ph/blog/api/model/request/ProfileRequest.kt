package com.ph.blog.api.model.request

import com.ph.blog.api.model.Profile
import javax.validation.constraints.Email
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

data class ProfileRequest(@field:NotNull(message = "first name can't be null or empty")
                          @field:NotEmpty(message = "first name can't be null or empty")
                          val firstName: String,
                          @field:NotNull(message = "last name can't be null or empty")
                          @field:NotEmpty(message = "last name can't be null or empty")
                          val lastName: String,
                          @field:NotNull(message = "username can't be null or empty")
                          @field:NotEmpty(message = "username can't be null or empty")
                          val userName: String,
                          @field:Email(message = "Wrong email")
                          @field:NotNull(message = "email can't be null or empty")
                          @field:NotEmpty(message = "email can't be null or empty")
                          val email: String,
                          @field:NotNull(message = "password can't be null or empty")
                          @field:NotEmpty(message = "password can't be null or empty")
                          val password: String,
                          val isAuthor: Boolean)
