package com.ph.blog.api.model.response

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity

class SuccessResponseEntity: ResponseEntity<SuccessResponse> {

    constructor(body: SuccessResponse): super(body, body.status)

    companion object {
        fun success(message: String)=
                SuccessResponseEntity(SuccessResponse(HttpStatus.OK, message))

        fun accepted(message: String)=
                SuccessResponseEntity(SuccessResponse(HttpStatus.ACCEPTED, message))
    }
}
