package com.ph.blog.api.model

import com.ph.blog.api.model.request.ArticleRequest
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.LocalDate

@Document
data class Article(@Id val id: String? = null,
                   var blogHtml: String? = null,
                   var title: String? = null,
                   var shortDescription: String? = null,
                   var postDate: LocalDate? = LocalDate.now(),
                   var author: String? = null,
                   var category: String? = null,
                   var comments: MutableList<Comment> = mutableListOf()) {

    fun addComment(comment: Comment) {
        comments.add(comment)
    }

    fun replaceContent(articleRequest: ArticleRequest) {
        this.blogHtml = articleRequest.blogHtml
        this.category = articleRequest.category
        this.title = articleRequest.title
        this.shortDescription = articleRequest.shortDescription
    }
}
