package com.ph.blog.api.model

import com.ph.blog.api.model.basic.BasicProfile
import com.ph.blog.api.model.request.ProfileRequest
import com.ph.blog.api.model.request.UpdateProfileRequest
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Profile(@Id val id: String? = null,
                   var firstName: String? = null,
                   var lastName: String? = null,
                   var userName: String? = null,
                   var email: String? = null,
                   var password: String? = null,
                   var isAuthor: Boolean = false,
                   var isEnabled: Boolean = true) {

    fun toBasic(): BasicProfile =
            BasicProfile(this.id!!, this.firstName, this.lastName, this.userName, this.email, this.isAuthor)

    fun replace(profileRequest: UpdateProfileRequest) {
        this.firstName = profileRequest.firstName
        this.lastName = profileRequest.lastName
        this.userName = profileRequest.userName
        this.email = profileRequest.email
    }
}
