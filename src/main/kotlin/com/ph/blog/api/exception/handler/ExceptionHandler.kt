package com.ph.blog.api.exception.handler

import com.ph.blog.api.exception.*
import com.ph.blog.api.exception.handler.ErrorResponseEntity.Companion.badRequest
import com.ph.blog.api.exception.handler.ErrorResponseEntity.Companion.blockUser
import com.ph.blog.api.exception.handler.ErrorResponseEntity.Companion.notFound
import com.ph.blog.api.exception.handler.ErrorResponseEntity.Companion.serverError
import com.ph.blog.api.exception.handler.ErrorResponseEntity.Companion.unauthorized
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.io.PrintWriter
import java.io.StringWriter
import java.util.*

@ControllerAdvice
class ExceptionHandler {

    private val logger = LoggerFactory.getLogger(com.ph.blog.api.exception.handler.ExceptionHandler::class.java)
    private val sw = StringWriter()

    @ExceptionHandler(ArticleNotFoundException::class, ProfileNotFoundException::class)
    fun entityNotFoundException(exception: RuntimeException, locale: Locale): ErrorResponseEntity {
        exception.printStackTrace(PrintWriter(sw))
        logger.error(sw.toString())
        return notFound("Entity Not Found!")
    }

    @ExceptionHandler(Exception::class)
    fun unknownException(exception: Exception, locale: Locale): ErrorResponseEntity {
        exception.printStackTrace(PrintWriter(sw))
        logger.error(sw.toString())
        return serverError("Internal Server Error")
    }

    @ExceptionHandler(LoginAttemptsException::class, AuthenticationException::class)
    fun blockedUser(exception: LoginAttemptsException, locale: Locale): ErrorResponseEntity {
        exception.printStackTrace(PrintWriter(sw))
        logger.error(sw.toString())
        return blockUser("user is blocked by to many attempts")
    }

    @ExceptionHandler(TitleAlreadyExistException::class, ProfileAlreadyExist::class)
    fun entityAlreadyExist(exception: RuntimeException, locale: Locale): ErrorResponseEntity {
        exception.printStackTrace(PrintWriter(sw))
        logger.error(sw.toString())
        return badRequest("entity already exist!")
    }

    @ExceptionHandler(BadCredentialsException::class)
    fun wrongCredentials(exception: BadCredentialsException, locale: Locale): ErrorResponseEntity {
        exception.printStackTrace(PrintWriter(sw))
        logger.error(sw.toString())
        return unauthorized("User Or Password Wrong")
    }

    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun validation(exception: MethodArgumentNotValidException, locale: Locale): ErrorResponseEntity {
        exception.printStackTrace(PrintWriter(sw))
        logger.error(sw.toString())
        return badRequest("Wrong data given")
    }

}
