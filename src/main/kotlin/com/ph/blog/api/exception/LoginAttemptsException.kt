package com.ph.blog.api.exception

import org.springframework.security.core.AuthenticationException

class LoginAttemptsException(message: String) : AuthenticationException(message)
