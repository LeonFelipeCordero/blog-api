package com.ph.blog.api.exception

class TitleAlreadyExistException(message: String) : RuntimeException(message)
