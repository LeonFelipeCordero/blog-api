package com.ph.blog.api.exception

import org.springframework.http.HttpStatus
import java.time.LocalDateTime
import java.util.*

data class ErrorResponse(val status: HttpStatus,
                         val error: String? = null,
                         val message: String? = null,
                         val timestamp: LocalDateTime? = null,
                         val bindingErrors: List<String>? = null) {

    constructor(status: HttpStatus, message: String, bindingErrors: List<String>) :
            this(status, status.reasonPhrase, message, LocalDateTime.now(), bindingErrors)

    constructor(status: HttpStatus, error: String, message: String) :
            this(status, error, message, LocalDateTime.now(), ArrayList<String>())

    constructor(status: HttpStatus, message: String) :
            this(status, status.reasonPhrase, message, LocalDateTime.now(), ArrayList<String>())

}
