package com.ph.blog.api.exception.handler

import com.ph.blog.api.exception.ErrorResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.MultiValueMap

class ErrorResponseEntity(body: ErrorResponse) : ResponseEntity<ErrorResponse>(body, body.status) {

    companion object {

        fun badRequest(message: String) =
                ErrorResponseEntity(ErrorResponse(HttpStatus.BAD_REQUEST, message))

        fun notFound(message: String) =
                ErrorResponseEntity(ErrorResponse(HttpStatus.NOT_FOUND, message))

        fun serverError(message: String) =
                ErrorResponseEntity(ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, message))

        fun unauthorized(message: String) =
                ErrorResponseEntity(ErrorResponse(HttpStatus.UNAUTHORIZED, message))

        fun blockUser(message: String) =
                ErrorResponseEntity(ErrorResponse(HttpStatus.FORBIDDEN, message))
    }
}
