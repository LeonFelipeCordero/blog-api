package com.ph.blog.api.exception

import java.lang.RuntimeException

class ProfileNotFoundException(message: String) : RuntimeException(message)
