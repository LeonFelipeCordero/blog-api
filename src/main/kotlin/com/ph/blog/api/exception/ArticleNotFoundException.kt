package com.ph.blog.api.exception

import java.lang.RuntimeException

class ArticleNotFoundException(message: String) : RuntimeException(message)
