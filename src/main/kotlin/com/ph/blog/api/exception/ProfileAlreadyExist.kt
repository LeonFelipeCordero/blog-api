package com.ph.blog.api.exception

import java.lang.RuntimeException

class ProfileAlreadyExist(message: String) : RuntimeException(message)
