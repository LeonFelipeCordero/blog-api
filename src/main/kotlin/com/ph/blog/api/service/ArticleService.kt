package com.ph.blog.api.service

import com.ph.blog.api.exception.ArticleNotFoundException
import com.ph.blog.api.exception.TitleAlreadyExistException
import com.ph.blog.api.model.Article
import com.ph.blog.api.model.request.ArticleRequest
import com.ph.blog.api.model.request.CommentRequest
import com.ph.blog.api.repository.ArticleRepository
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.util.*

@Service
class ArticleService(private val articleRepository: ArticleRepository) {

    companion object {
        const val maxNumberOfArticles = 6
    }

    fun findArticleById(articleId: String): Article = articleRepository.findById(articleId).validate()

    fun findArticle(title: String?, author: String?, category: String?, date: LocalDate?): List<Article> {
        val articles = mutableSetOf<Article>()
        title?.let {
            articles.addAll(articleRepository.findArticleByTitleContaining(title).orEmpty())
        }
        articles.addAll(articleRepository
                .findArticleByAuthorOrCategoryOrPostDateAfter(author, category, date).orEmpty())
        return articles.toList().ifEmpty { throw ArticleNotFoundException("Article Not Found") }
    }

    fun save(articleRequest: ArticleRequest): Article {
        articleRepository.findArticleByTitle(articleRequest.title!!).validateTitle()
        return articleRepository.save(articleRequest.toArticle())
    }

    fun save(articleRequest: ArticleRequest, articleId: String): Article {
        val article = findArticleById(articleId)
        article.replaceContent(articleRequest)
        return articleRepository.save(article)
    }

    fun addComment(id: String, commentRequest: CommentRequest) {
        val article = findArticleById(id)
        article.addComment(commentRequest.comment)
        articleRepository.save(article)
    }

    fun findTopByPostDateAfter(date: LocalDate): List<Article> {
        var articles = articleRepository.findByPostDateAfter(date).orEmpty()
        articles = if (articles.size < maxNumberOfArticles) articles.subList(0, articles.size) else articles.subList(0, 5)
        return articles.ifEmpty { throw ArticleNotFoundException("Article Not Found") }
    }

    fun deleteArticle(articleId: String) {
        findArticleById(articleId)
        articleRepository.deleteById(articleId)
    }
}

fun Optional<Article>.validate() = this.orElse(null) ?: throw ArticleNotFoundException("Article Not Found")

fun Article?.validateTitle() =
        this?.apply { throw TitleAlreadyExistException("Article Title Already Exist") }
