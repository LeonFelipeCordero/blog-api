package com.ph.blog.api.service

import com.ph.blog.api.exception.ProfileAlreadyExist
import com.ph.blog.api.exception.ProfileNotFoundException
import com.ph.blog.api.model.Profile
import com.ph.blog.api.model.basic.BasicProfile
import com.ph.blog.api.model.request.ProfileRequest
import com.ph.blog.api.model.request.UpdateProfileRequest
import com.ph.blog.api.repository.ProfileRepository
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class ProfileService(private val profileRepository: ProfileRepository,
                     private val passwordEncoder: PasswordEncoder) {

    fun findProfileById(id: String): Profile = profileRepository.findById(id).validate()

    fun findProfileByEmail(email: String): Profile = profileRepository.findProfileByEmail(email).validate()

    fun findBasicProfileByEmail(email: String): BasicProfile = findProfileByEmail(email).toBasic()

    fun save(profileRequest: ProfileRequest): BasicProfile {
        profileRepository.findProfileByEmailOrUserName(profileRequest.email, profileRequest.userName).assertNotExist()
        return profileRepository.save(requestToProfile(profileRequest)).toBasic()
    }

    fun save(profileRequest: UpdateProfileRequest, id: String): BasicProfile {
        val profile = findProfileById(id)
        profile.replace(profileRequest)
        return profileRepository.save(profile).toBasic()
    }

    fun delete(id: String) {
        findProfileById(id)
        profileRepository.deleteById(id)
    }

    fun updateToAuthor(id: String): BasicProfile {
        val profile = findProfileById(id)
        profile.isAuthor = true
        return profileRepository.save(profile).toBasic()
    }

    private fun requestToProfile(profileRequest: ProfileRequest) =
            Profile(firstName = profileRequest.firstName,
                    lastName = profileRequest.lastName,
                    userName = profileRequest.userName,
                    email = profileRequest.email,
                    password = passwordEncoder.encode(profileRequest.password),
                    isAuthor = profileRequest.isAuthor,
                    isEnabled = true)
}

fun Optional<Profile>.validate(): Profile =
        this.orElse(null) ?: throw ProfileNotFoundException("Profile not found")

fun Profile?.validate(): Profile =
        this ?: throw throw ProfileNotFoundException("Profile not found")

fun Profile?.assertNotExist() =
        this?.let { throw ProfileAlreadyExist("Profile email already exist") }
