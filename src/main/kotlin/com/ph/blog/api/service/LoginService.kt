package com.ph.blog.api.service

import com.ph.blog.api.auth.LoginAttemptService
import com.ph.blog.api.model.basic.BasicProfile
import com.ph.blog.api.model.request.LoginRequest
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class LoginService(private val profileService: ProfileService,
                   private val loginAttemptService: LoginAttemptService,
                   private val authenticationManager: AuthenticationManager) {

    fun login(loginRequest: LoginRequest): BasicProfile =
            try {
                val authentication =
                        authenticate(UsernamePasswordAuthenticationToken(loginRequest.email, loginRequest.password))
                val email = (authentication.principal as com.ph.blog.api.model.Profile).email!!
                loginAttemptService.approvedLogin(email)
                profileService.findBasicProfileByEmail(email)
            } catch (e: BadCredentialsException) {
                loginAttemptService.failedLogin(loginRequest.email)
                throw BadCredentialsException("User Or Password wrong")
            }


    private fun authenticate(authToken: Authentication): Authentication {
        val authentication = authenticationManager.authenticate(authToken)
        SecurityContextHolder.getContext().authentication = authentication
        return authentication
    }

}
