package com.ph.blog.api.auth

import com.ph.blog.api.service.ProfileService
import com.ph.blog.api.service.validate
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.core.Authentication

class AuthenticationProvider(private val profileService: ProfileService) : DaoAuthenticationProvider() {

    override fun authenticate(auth: Authentication) : Authentication {
        val profile = profileService.findProfileByEmail(auth.name).validate()
        val result = super.authenticate(auth)
        return UsernamePasswordAuthenticationToken(profile, result.credentials, result.authorities)
    }

    override fun supports(authentication: Class<*>): Boolean {
        return authentication == UsernamePasswordAuthenticationToken::class.java
    }
}
