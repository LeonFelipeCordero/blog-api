package com.ph.blog.api.auth

import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit

@Service
class LoginAttemptService {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(LoginAttemptService::class.java)
        private const val maxAttempts = 10
    }

    private val attemptsCache: LoadingCache<String, Int> =
            CacheBuilder
                    .newBuilder()
                    .expireAfterWrite(1, TimeUnit.DAYS)
                    .build(object : CacheLoader<String, Int>() {
                        override fun load(key: String): Int? {
                            return 0
                        }
                    })


    fun approvedLogin(key: String) = attemptsCache.invalidate(key)

    fun failedLogin(key: String) =
            try {
                val attempts = getAttempts(key)
                attemptsCache.put(key, attempts + 1)
            } catch (e: ExecutionException) {
                logger.error(e.message)
                attemptsCache.put(key, 1)
            }

    fun isBlocked(key: String): Boolean =
            try {
                val attempts = getAttempts(key)
                attempts > maxAttempts
            } catch (e: ExecutionException) {
                logger.error(e.message)
                false
            }

    fun getAttempts(key: String): Int = attemptsCache.get(key) ?: 0
}
