package com.ph.blog.api.auth

import com.ph.blog.api.exception.LoginAttemptsException
import com.ph.blog.api.service.ProfileService
import com.ph.blog.api.service.validate
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service

@Service("userDetailsService")
class BlogUserDetailsService(private val profileService: ProfileService,
                             private val loginAttemptService: LoginAttemptService) : UserDetailsService {


    override fun loadUserByUsername(email: String): UserDetails {
        validateAttempts(email)
        val profile = profileService.findProfileByEmail(email).validate()
        return User(profile.email, profile.password, profile.isEnabled,
                true, true, true, emptyList())

    }

    private fun validateAttempts(email: String) {
        if (loginAttemptService.isBlocked(email)) {
            throw LoginAttemptsException("User login blocked")
        }
    }

}
