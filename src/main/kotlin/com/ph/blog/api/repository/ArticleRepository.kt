package com.ph.blog.api.repository

import com.ph.blog.api.model.Article
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface ArticleRepository : MongoRepository<Article, String> {

    fun findArticleByTitle(title: String): Article?

    fun findArticleByTitleContaining(title: String?): List<Article>?

    fun findArticleByAuthorOrCategoryOrPostDateAfter(author: String?,
                                                     category: String?,
                                                     dateTime: LocalDate?): List<Article>?

    fun findByPostDateAfter(localDate: LocalDate): List<Article>?
}
