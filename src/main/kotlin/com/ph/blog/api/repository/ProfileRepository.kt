package com.ph.blog.api.repository

import com.ph.blog.api.model.Profile
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface ProfileRepository : MongoRepository<Profile, String> {

    fun findProfileByEmail(email: String): Profile?
    fun findProfileByEmailOrUserName(email: String, userName: String): Profile?
}
