package com.ph.blog.api.parsers

import com.ph.blog.api.model.Article
import com.ph.blog.api.model.Author
import com.ph.blog.api.model.Category
import com.ph.blog.api.model.Profile
import com.ph.blog.api.model.request.ArticleRequest
import com.ph.blog.api.model.request.ProfileRequest
import java.time.LocalDateTime

object ModelParser {

    fun buildProfileRequest(): ProfileRequest =
            ProfileRequest(firstName = "leon",
                    lastName = "cordero",
                    userName = "leon cordero",
                    email = "email@emial.com",
                    password = "1234",
                    isAuthor = false)

    fun buildProfile(id: String): Profile =
            Profile(id, "name", "lastName", "user", "test@email.com",
                    "123123123", true, true)
}
