package com.ph.blog.api

import com.ph.blog.api.exception.ErrorResponse
import com.ph.blog.api.model.Article
import com.ph.blog.api.model.Profile
import com.ph.blog.api.model.basic.BasicProfile
import com.ph.blog.api.model.response.SuccessResponse
import io.restassured.RestAssured.given
import io.restassured.http.ContentType
import io.restassured.specification.RequestSender
import org.apache.commons.io.FileUtils
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import java.io.File
import java.nio.charset.StandardCharsets

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = [
            "spring.cloud.discovery.client.simple.instances.profile[0].uri=http://localhost:\${wiremock.server.port}"
        ]
)
abstract class IntegrationTestBase {

    @LocalServerPort
    protected var port: Int = 0

    fun createProfile(): BasicProfile =
            baseRequest(202, fileToString("/mock/profile/newProfileRequest.json"))
                    .post("/profile")
                    .then()
                    .extract()
                    .`as`(BasicProfile::class.java)

    fun createAuthorProfile(): BasicProfile =
            baseRequest(202, fileToString("/mock/profile/newProfileAuthorRequest.json"))
                    .post("/profile")
                    .then()
                    .extract()
                    .`as`(BasicProfile::class.java)

    fun findProfileById(id: String): Profile =
            baseRequest(200)
                    .get("/profile/$id")
                    .then()
                    .extract()
                    .`as`(Profile::class.java)


    fun deleteProfile(id: String): SuccessResponse =
            baseRequest(202)
                    .delete("/profile/$id")
                    .then()
                    .extract()
                    .`as`(SuccessResponse::class.java)

    fun becomeAuthor(id: String): BasicProfile =
            baseRequest(200)
                    .put("/profile/$id/author")
                    .then()
                    .extract()
                    .`as`(BasicProfile::class.java)

    fun updateProfile(id: String): BasicProfile =
            baseRequest(202, fileToString("/mock/profile/updateProfileRequest.json"))
                    .put("profile/$id")
                    .then()
                    .extract()
                    .`as`(BasicProfile::class.java)

    fun createExistingUser(): ErrorResponse =
            baseRequest(400, fileToString("/mock/profile/newProfileRequest.json"))
                    .post("/profile")
                    .then()
                    .extract()
                    .`as`(ErrorResponse::class.java)

    fun findNotExistingUser(): ErrorResponse =
            baseRequest(404)
                    .get("/profile/123")
                    .then()
                    .extract()
                    .`as`(ErrorResponse::class.java)

    fun deleteNotExistingUser(): ErrorResponse =
            baseRequest(404)
                    .delete("/profile/123")
                    .then()
                    .extract()
                    .`as`(ErrorResponse::class.java)

    fun updateNotExistingProfile(): ErrorResponse =
            baseRequest(404, fileToString("/mock/profile/updateProfileRequest.json"))
                    .put("/profile/123")
                    .then()
                    .extract()
                    .`as`(ErrorResponse::class.java)

    fun becomeAuthorNotExitingProfile(): ErrorResponse =
            baseRequest(404)
                    .put("/profile/123/author")
                    .then()
                    .extract()
                    .`as`(ErrorResponse::class.java)

    fun createArticle(title: String, author: String): Article =
            baseRequest(202,
                    fileToString("/mock/article/newArticleRequest.json")
                            .replace("\$title", title)
                            .replace("\$author", author))
                    .post("/article")
                    .then()
                    .extract()
                    .`as`(Article::class.java)

    fun findArticle(id: String): Article =
            baseRequest(200)
                    .get("/article/$id")
                    .then()
                    .extract()
                    .`as`(Article::class.java)

    fun deleteArticle(id: String): SuccessResponse =
            baseRequest(202)
                    .delete("/article/$id")
                    .then()
                    .extract()
                    .`as`(SuccessResponse::class.java)

    fun updateArticle(id: String): Article =
            baseRequest(202, fileToString("/mock/article/updateArticleRequest.json"))
                    .put("/article/$id")
                    .then()
                    .extract()
                    .`as`(Article::class.java)

    fun addComment(id: String): SuccessResponse =
            baseRequest(202, fileToString("/mock/article/addComment.json"))
                    .put("/article/$id/comment")
                    .then()
                    .extract()
                    .`as`(SuccessResponse::class.java)

    fun addCommentLogged(id: String, profile: String): SuccessResponse =
            baseRequest(202,
                    fileToString("/mock/article/addCommentLogged.json")
                            .replace("\$profile", profile))
                    .put("/article/$id/comment")
                    .then()
                    .extract()
                    .`as`(SuccessResponse::class.java)


    fun findArticleBy(parameter: String, value: String): List<Article> =
            baseRequest(200)
                    .get("/article?$parameter=$value")
                    .then()
                    .extract()
                    .body()
                    .jsonPath().getList(".", Article::class.java)

    fun login(): BasicProfile =
            baseRequest(200, fileToString("/mock/login/success.json"))
                    .put("/login")
                    .then()
                    .extract()
                    .`as`(BasicProfile::class.java)

    fun failLogin(): ErrorResponse =
            baseRequest(401, fileToString("/mock/login/fail.json"))
                    .put("/login")
                    .then()
                    .extract()
                    .`as`(ErrorResponse::class.java)

    fun blockLogin(): ErrorResponse =
            baseRequest(403, fileToString("/mock/login/fail.json"))
                    .put("/login")
                    .then()
                    .extract()
                    .`as`(ErrorResponse::class.java)

    private fun baseRequest(expectedStatus: Int, body: String? = null): RequestSender {
        val base = given()
                .request()
                .port(port)
                .contentType(ContentType.JSON)
        body?.let { base.body(body) }
        return base
                .expect()
                .statusCode(expectedStatus)
                .`when`()
    }

    private fun fileToString(filePath: String) =
            FileUtils.readFileToString(File(this.javaClass.getResource(filePath).file), StandardCharsets.UTF_8.toString())

}
