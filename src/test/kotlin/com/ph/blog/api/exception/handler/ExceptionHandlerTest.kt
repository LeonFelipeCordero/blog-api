package com.ph.blog.api.exception.handler

import com.ph.blog.api.exception.LoginAttemptsException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import org.springframework.security.authentication.BadCredentialsException
import java.util.*


internal class ExceptionHandlerTest {

    private val exceptionHandler = ExceptionHandler()

    @Test
    fun `should return entity not found error`() {
        val error = exceptionHandler.entityNotFoundException(RuntimeException(), Locale.ENGLISH)
        assertThat(error.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
        assertThat(error.body!!.message).isEqualTo("Entity Not Found!")
    }

    @Test
    fun `should return unknown error`() {
        val error = exceptionHandler.unknownException(RuntimeException(), Locale.ENGLISH)
        assertThat(error.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
        assertThat(error.body!!.message).isEqualTo("Internal Server Error")
    }

    @Test
    fun `should return blocked user error`() {
        val error = exceptionHandler.blockedUser(LoginAttemptsException(""), Locale.ENGLISH)
        assertThat(error.statusCode).isEqualTo(HttpStatus.FORBIDDEN)
        assertThat(error.body!!.message).isEqualTo("user is blocked by to many attempts")
    }

    @Test
    fun `should return entity already exist error`() {
        val error = exceptionHandler.entityAlreadyExist(RuntimeException(), Locale.ENGLISH)
        assertThat(error.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
        assertThat(error.body!!.message).isEqualTo("entity already exist!")
    }

    @Test
    fun `should return wrong credentials error`() {
        val error = exceptionHandler.wrongCredentials(BadCredentialsException(""), Locale.ENGLISH)
        assertThat(error.statusCode).isEqualTo(HttpStatus.UNAUTHORIZED)
        assertThat(error.body!!.message).isEqualTo("User Or Password Wrong")
    }
}
