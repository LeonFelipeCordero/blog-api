package com.ph.blog.api.exception.handler

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus

internal class ErrorResponseEntityTest {


    @Test
    fun `should return bad request`() {
        val error = ErrorResponseEntity.badRequest("bad request")
        assertThat(error.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
    }

    @Test
    fun `should return not found`() {
        val error = ErrorResponseEntity.notFound("not found")
        assertThat(error.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
    }

    @Test
    fun `should return server error`() {
        val error = ErrorResponseEntity.serverError("server error")
        assertThat(error.statusCode).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)
    }

    @Test
    fun `should return unauthorized`() {
        val error = ErrorResponseEntity.unauthorized("unauthorized")
        assertThat(error.statusCode).isEqualTo(HttpStatus.UNAUTHORIZED)
    }

    @Test
    fun `should return blocked user`() {
        val error = ErrorResponseEntity.blockUser("blocked user")
        assertThat(error.statusCode).isEqualTo(HttpStatus.FORBIDDEN)
    }
}
