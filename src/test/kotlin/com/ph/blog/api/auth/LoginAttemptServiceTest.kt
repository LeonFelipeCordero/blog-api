package com.ph.blog.api.auth

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class LoginAttemptServiceTest {

    companion object {
        private const val key = "key"
    }

    private val loginAttemptService = LoginAttemptService()

    @Test
    fun `should approve login after 1 failed attempt`() {
        loginAttemptService.failedLogin(key)
        var attempts = loginAttemptService.getAttempts(key)
        assertThat(attempts).isOne()

        loginAttemptService.approvedLogin(key)
        attempts = loginAttemptService.getAttempts(key)
        assertThat(attempts).isZero()
    }

    @Test
    fun `should fail login and rest quota`() {
        loginAttemptService.failedLogin(key)
        val attempts = loginAttemptService.getAttempts(key)
        assertThat(attempts).isOne()
    }

    @Test
    fun `should block user after max attempts`() {
        for(i in 1..11) {
            loginAttemptService.failedLogin(key)
        }
        val blocked = loginAttemptService.isBlocked(key)
        assertThat(blocked).isTrue()
    }

    @Test
    fun `should not user before max attempts`() {
        for(i in 1..9) {
            loginAttemptService.failedLogin(key)
            val blocked = loginAttemptService.isBlocked(key)
            assertThat(blocked).isFalse()
        }
        val blocked = loginAttemptService.isBlocked(key)
        assertThat(blocked).isFalse()
    }
}
