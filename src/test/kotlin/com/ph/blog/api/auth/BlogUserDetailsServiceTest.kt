package com.ph.blog.api.auth

import com.ph.blog.api.exception.LoginAttemptsException
import com.ph.blog.api.parsers.ModelParser.buildProfile
import com.ph.blog.api.service.ProfileService
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

internal class BlogUserDetailsServiceTest {

    companion object {
        private const val email = "email"
        private const val profileId = "1L"
    }

    @Mock
    private lateinit var profileService: ProfileService

    @Mock
    private val loginAttemptService = LoginAttemptService()

    private lateinit var blogUserDetailsService: BlogUserDetailsService

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        blogUserDetailsService = BlogUserDetailsService(profileService, loginAttemptService)
    }

    @Test
    fun `should load user by username`() {
        val profile = buildProfile(profileId)
        Mockito.`when`(profileService.findProfileByEmail(email)).thenReturn(profile)
        Mockito.`when`(loginAttemptService.isBlocked(email)).thenReturn(false)

        val user = blogUserDetailsService.loadUserByUsername(email)
        assertThat(user.username).isEqualTo(profile.email)
        assertThat(user.password).isEqualTo(profile.password)
        assertThat(user.isEnabled).isEqualTo(profile.isEnabled)
        assertThat(user.isAccountNonExpired).isTrue()
        assertThat(user.isCredentialsNonExpired).isTrue()
        assertThat(user.isAccountNonLocked).isTrue()
        assertThat(user.authorities).isEmpty()
    }

    @Test
    fun `should not load user by username`() {
        val profile = buildProfile(profileId)
        Mockito.`when`(profileService.findProfileByEmail(email)).thenReturn(profile)
        Mockito.`when`(loginAttemptService.isBlocked(email)).thenReturn(true)
        assertThatExceptionOfType(LoginAttemptsException::class.java)
                .isThrownBy { blogUserDetailsService.loadUserByUsername(email) }
    }
}
