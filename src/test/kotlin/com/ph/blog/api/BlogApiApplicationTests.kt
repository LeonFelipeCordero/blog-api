package com.ph.blog.api

import com.ph.blog.api.controller.ArticleController
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

class BlogApiApplicationTests : IntegrationTestBase() {

    @Autowired
    private lateinit var articleController: ArticleController

    @Test
    fun contextLoads() {
        assertThat(articleController).isNotNull
    }

}
