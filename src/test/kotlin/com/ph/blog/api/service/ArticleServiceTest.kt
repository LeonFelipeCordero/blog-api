package com.ph.blog.api.service

import com.nhaarman.mockitokotlin2.verify
import com.ph.blog.api.exception.ArticleNotFoundException
import com.ph.blog.api.exception.TitleAlreadyExistException
import com.ph.blog.api.model.Article
import com.ph.blog.api.model.Comment
import com.ph.blog.api.model.basic.BasicProfile
import com.ph.blog.api.model.request.ArticleRequest
import com.ph.blog.api.model.request.CommentRequest
import com.ph.blog.api.repository.ArticleRepository
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class ArticleServiceTest {

    private val articleId = "1L"
    private val profileId = "1L"
    private val articleTitle = "Title"
    private val profileEmail = "email@test.com"

    private var articleRepository = mock(ArticleRepository::class.java)
    private var profileService = mock(ProfileService::class.java)

    private lateinit var service: ArticleService

    @BeforeEach
    fun setUp() {
        service = ArticleService(articleRepository)

        `when`(articleRepository.save(any(Article::class.java))).thenReturn(Article())
    }

    @Test
    fun `should save article when title is not present`() {
        `when`(articleRepository.findArticleByTitle(articleTitle)).thenReturn(null)
        `when`(profileService.findBasicProfileByEmail(profileEmail))
                .thenReturn(BasicProfile(id = profileId, email = profileEmail))
        service.save(ArticleRequest(articleTitle, profileEmail))
        verify(articleRepository).save(any(Article::class.java))
    }

    @Test
    fun `should not save article when title is present`() {
        `when`(articleRepository.findArticleByTitle(articleTitle))
                .thenReturn(Article(id = articleId, title = articleTitle))
        assertThatExceptionOfType(TitleAlreadyExistException::class.java)
                .isThrownBy {
                    service.save(ArticleRequest(articleTitle))
                }
        verify(articleRepository, times(0)).save(any(Article::class.java))
    }

    @Test
    fun `should save article update when it exist`() {
        `when`(articleRepository.findById(articleId))
                .thenReturn(Optional.of(Article(id = articleId, title = articleTitle)))
        service.save(ArticleRequest(articleTitle, profileEmail), articleId)
        verify(articleRepository).save(any(Article::class.java))
    }

    @Test
    fun `should not save article update when it does not exist`() {
        `when`(articleRepository.findById(articleTitle)).thenReturn(Optional.empty())
        assertThatExceptionOfType(ArticleNotFoundException::class.java)
                .isThrownBy {
                    service.save(ArticleRequest(articleTitle), articleId)
                }
        verify(articleRepository, times(0)).save(any(Article::class.java))
    }

    @Test
    fun `should find article by id`() {
        `when`(articleRepository.findById(articleId)).thenReturn(Optional.of(Article()))
        val result = service.findArticleById(articleId)
        assertThat(result).isNotNull
    }

    @Test
    fun `should not find article by id`() {
        `when`(articleRepository.findById(articleId)).thenReturn(Optional.empty())
        assertThatExceptionOfType(ArticleNotFoundException::class.java)
                .isThrownBy {
                    service.findArticleById(articleId)
                }
    }

    @Test
    fun `should find article my combination with title`() {
        `when`(articleRepository.findArticleByTitleContaining(any())).thenReturn(listOf(Article()))
        `when`(articleRepository.findArticleByAuthorOrCategoryOrPostDateAfter(any(), any(), any()))
                .thenReturn(listOf())
        val result = service.findArticle(articleId, profileEmail, "", LocalDate.now())
        assertThat(result).hasSize(1)
    }

    @Test
    fun `should not find article my combination with title no params`() {
        `when`(articleRepository.findArticleByTitleContaining(any())).thenReturn(listOf())
        `when`(articleRepository.findArticleByAuthorOrCategoryOrPostDateAfter(any(), any(), any()))
                .thenReturn(listOf())
        assertThatExceptionOfType(ArticleNotFoundException::class.java)
                .isThrownBy {
                    service.findArticle(articleId, profileEmail, "", LocalDate.now())
                }
    }

    @Test
    fun `should find article my combination with params`() {
        `when`(articleRepository.findArticleByTitleContaining(any())).thenReturn(listOf())
        `when`(articleRepository.findArticleByAuthorOrCategoryOrPostDateAfter(any(), any(), any()))
                .thenReturn(listOf(Article()))
        val result = service.findArticle(articleId, profileEmail, "", LocalDate.now())
        assertThat(result).hasSize(1)
    }

    @Test
    fun `should not find article my combination with title and params`() {
        `when`(articleRepository.findArticleByTitleContaining(any())).thenReturn(listOf(Article(id = "1")))
        `when`(articleRepository.findArticleByAuthorOrCategoryOrPostDateAfter(any(), any(), any()))
                .thenReturn(listOf(Article(id = "2")))
        val result = service.findArticle(articleId, profileEmail, "", LocalDate.now())
        assertThat(result).hasSize(2)
    }

    @Test
    fun `should find article my combination and filter repeated`() {
        `when`(articleRepository.findArticleByTitleContaining(any())).thenReturn(listOf(Article(id = "1")))
        `when`(articleRepository.findArticleByAuthorOrCategoryOrPostDateAfter(any(), any(), any()))
                .thenReturn(listOf(Article(id = "1")))
        val result = service.findArticle(articleId, profileEmail, "", LocalDate.now())
        assertThat(result).hasSize(1)
    }

    @Test
    fun `should add comments to article`() {
        `when`(articleRepository.findById(articleId))
                .thenReturn(Optional.of(Article(id = articleId, title = articleTitle)))
        service.addComment(articleId, CommentRequest(Comment()))
        verify(articleRepository).save(any(Article::class.java))
    }

    @Test
    fun `should not add comments to article`() {
        `when`(articleRepository.findById(articleId)).thenReturn(Optional.empty())
        assertThatExceptionOfType(ArticleNotFoundException::class.java)
                .isThrownBy {
                    service.addComment(articleId, CommentRequest(Comment()))
                }
    }

    @Test
    fun `should find top 3 articles`() {
        val date = LocalDate.now().minusDays(1)
        `when`(articleRepository.findByPostDateAfter(date))
                .thenReturn(listOf(Article(), Article(), Article()))
        val result = service.findTopByPostDateAfter(date)
        assertThat(result).hasSize(3)
    }

    @Test
    fun `should find top 5 articles`() {
        val date = LocalDate.now().minusDays(1)
        `when`(articleRepository.findByPostDateAfter(date))
                .thenReturn(listOf(Article(), Article(), Article(), Article(), Article(), Article()))
        val result = service.findTopByPostDateAfter(date)
        assertThat(result).hasSize(5)
    }

    @Test
    fun `should not find top articles`() {
        `when`(articleRepository.findByPostDateAfter(LocalDate.now().minusDays(1)))
                .thenReturn(emptyList())
        assertThatExceptionOfType(ArticleNotFoundException::class.java)
                .isThrownBy {
                    service.findTopByPostDateAfter(LocalDate.now().minusDays(1))
                }
    }
}
