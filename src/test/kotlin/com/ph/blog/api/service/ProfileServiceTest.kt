package com.ph.blog.api.service

import com.nhaarman.mockitokotlin2.verify
import com.ph.blog.api.exception.ArticleNotFoundException
import com.ph.blog.api.exception.ProfileAlreadyExist
import com.ph.blog.api.exception.ProfileNotFoundException
import com.ph.blog.api.model.Article
import com.ph.blog.api.model.Profile
import com.ph.blog.api.parsers.ModelParser.buildProfile
import com.ph.blog.api.parsers.ModelParser.buildProfileRequest
import com.ph.blog.api.repository.ProfileRepository
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.springframework.security.crypto.password.PasswordEncoder
import java.util.*

internal class ProfileServiceTest {

    companion object {
        private const val email = "email"
        private const val profileId = "1L"
    }

    private var profileRepository = mock(ProfileRepository::class.java)

    private var passwordEncoder = mock(PasswordEncoder::class.java)

    private lateinit var profileService: ProfileService

    @BeforeEach
    fun setUp() {
        `when`(profileRepository.save(any(Profile::class.java))).thenReturn(Profile(id = profileId, email = email))
        profileService = ProfileService(profileRepository, passwordEncoder)
    }

    @Test
    fun `should find article by id`() {
        `when`(profileRepository.findById(profileId)).thenReturn(Optional.of(Profile()))
        val result = profileService.findProfileById(profileId)
        assertThat(result).isNotNull
    }

    @Test
    fun `should not find article by id`() {
        `when`(profileRepository.findById(profileId)).thenReturn(Optional.empty())
        assertThatExceptionOfType(ProfileNotFoundException::class.java)
                .isThrownBy {
                    profileService.findProfileById(profileId)
                }
    }

    @Test
    fun `should find profile by email`() {
        `when`(profileRepository.findProfileByEmail(email)).thenReturn(buildProfile(profileId))

        val profile = profileService.findProfileByEmail(email)
        assertThat(profile.id).isEqualTo(profileId)
    }

    @Test
    fun `should not find profile by email`() {
        `when`(profileRepository.findProfileByEmail(email)).thenReturn(null)
        assertThatExceptionOfType(ProfileNotFoundException::class.java)
                .isThrownBy { profileService.findProfileByEmail(email) }
    }

    @Test
    fun `should find basic profile by email`() {
        `when`(profileRepository.findProfileByEmail(email)).thenReturn(buildProfile(profileId))

        val profile = profileService.findProfileByEmail(email)
        assertThat(profile.id).isEqualTo(profileId)
    }

    @Test
    fun `should not find basic profile by email`() {
        `when`(profileRepository.findProfileByEmail(email)).thenReturn(null)
        assertThatExceptionOfType(ProfileNotFoundException::class.java)
                .isThrownBy { profileService.findProfileByEmail(email) }
    }

    @Test
    fun `should save profile`() {
        val request = buildProfileRequest()
        `when`(profileRepository.findProfileByEmailOrUserName(request.email, request.userName))
                .thenReturn(null)
        profileService.save(request)
        verify(profileRepository).save(any(Profile::class.java))
    }

    @Test
    fun `should not save profile when email already exist`() {
        val request = buildProfileRequest()
        `when`(profileRepository.findProfileByEmailOrUserName(request.email, request.userName))
                .thenReturn(Profile(id = profileId, email = email))

        assertThatExceptionOfType(ProfileAlreadyExist::class.java)
                .isThrownBy { profileService.save(request) }
    }

    @Test
    fun `should delete profile if profile exist`() {
        `when`(profileRepository.findById(profileId)).thenReturn(Optional.of(buildProfile(profileId)))
        profileService.delete(profileId)
        verify(profileRepository).deleteById(profileId)
    }

    @Test
    fun `should not delete profile if profile do not exist`() {
        `when`(profileRepository.findById(profileId)).thenReturn(Optional.empty())
        assertThatExceptionOfType(ProfileNotFoundException::class.java)
                .isThrownBy { profileService.delete(profileId) }
    }
}
