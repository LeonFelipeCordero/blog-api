package com.ph.blog.api.service

import com.nhaarman.mockitokotlin2.any
import com.ph.blog.api.auth.LoginAttemptService
import com.ph.blog.api.exception.ProfileNotFoundException
import com.ph.blog.api.model.Profile
import com.ph.blog.api.model.basic.BasicProfile
import com.ph.blog.api.model.request.LoginRequest
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.*
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken

internal class LoginServiceTest {

    companion object {
        private const val email = "test@email.com"
    }

    private val profileService = mock(ProfileService::class.java)
    private val loginAttemptService = mock(LoginAttemptService::class.java)
    private val authenticationManager = mock(AuthenticationManager::class.java)

    private lateinit var loginService: LoginService

    @BeforeEach
    fun setUp() {
        loginService = LoginService(profileService, loginAttemptService, authenticationManager)
    }

    @Test
    fun `should log in`() {
        `when`(authenticationManager.authenticate(any())).thenReturn(createAuth())
        `when`(profileService.findBasicProfileByEmail(email)).thenReturn(createBasicProfile())

        val basicProfile = loginService.login(LoginRequest(email, "1234"))

        assertThat(basicProfile.email).isEqualTo(email)
    }

    @Test
    fun `should login but principal is wrong`() {
        `when`(authenticationManager.authenticate(any())).thenReturn(createAuth())
        `when`(profileService.findBasicProfileByEmail(email)).thenThrow(ProfileNotFoundException("not found"))

        assertThatExceptionOfType(ProfileNotFoundException::class.java)
                .isThrownBy { loginService.login(LoginRequest(email, "1234")) }
    }

    @Test
    fun `should throw bad credentials exception`() {
        `when`(authenticationManager.authenticate(any())).thenThrow(BadCredentialsException(""))
        assertThatExceptionOfType(BadCredentialsException::class.java)
                .isThrownBy { loginService.login(LoginRequest(email, "1234")) }
    }

    private fun createAuth(): UsernamePasswordAuthenticationToken =
            UsernamePasswordAuthenticationToken(
                    Profile(email = email),
                    "")

    private fun createBasicProfile(): BasicProfile =
            BasicProfile(email = email)

}
