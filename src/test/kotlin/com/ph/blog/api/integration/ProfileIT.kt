package com.ph.blog.api.integration

import com.ph.blog.api.IntegrationTestBase
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class ProfileIT : IntegrationTestBase() {

    @Test
    fun `should create reader user`() {
        val profile = createProfile()

        assertThat(profile.firstName).isEqualTo("Leon")
        assertThat(profile.lastName).isEqualTo("Cordero")
        assertThat(profile.userName).isEqualTo("LeonCordero")
        assertThat(profile.email).isEqualTo("leon@email.com")
        assertThat(profile.isAuthor).isEqualTo(false)

        deleteProfile(profile.id!!)
    }

    @Test
    fun `should not create user when email exist`() {
        val profile = createProfile()
        val error = createExistingUser()

        assertThat(error.message).isEqualTo("entity already exist!")

        deleteProfile(profile.id!!)
    }

    @Test
    fun `should create author user`() {
        val profile = createAuthorProfile()

        assertThat(profile.firstName).isEqualTo("Leon")
        assertThat(profile.lastName).isEqualTo("Cordero")
        assertThat(profile.userName).isEqualTo("LeonCordero")
        assertThat(profile.email).isEqualTo("leon@email.com")
        assertThat(profile.isAuthor).isEqualTo(true)

        deleteProfile(profile.id!!)
    }

    @Test
    fun `should find profile with same data`() {
        val profile = createProfile()

        val foundProfile = findProfileById(profile.id!!)

        assertThat(profile.firstName).isEqualTo(foundProfile.firstName)
        assertThat(profile.lastName).isEqualTo(foundProfile.lastName)
        assertThat(profile.userName).isEqualTo(foundProfile.userName)
        assertThat(profile.email).isEqualTo(foundProfile.email)
        assertThat(profile.isAuthor).isEqualTo(foundProfile.isAuthor)

        deleteProfile(profile.id!!)
    }

    @Test
    fun `should become author`() {
        val profile = createProfile()

        val author = becomeAuthor(profile.id!!)

        assertThat(profile.isAuthor).isNotEqualTo(author.isAuthor)

        deleteProfile(profile.id!!)
    }

    @Test
    fun `should update profile`() {
        val profile = createProfile()

        val updated = updateProfile(profile.id!!)

        assertThat(updated.firstName).isEqualTo("Not")
        assertThat(updated.lastName).isEqualTo("Me")
        assertThat(updated.userName).isEqualTo("NotMe")
        assertThat(updated.email).isEqualTo("not.me@email.com")

        deleteProfile(profile.id!!)
    }

    @Test
    fun `should not find profile`() {
        val error = findNotExistingUser()

        assertThat(error.message).isEqualTo("Entity Not Found!")
    }

    @Test
    fun `should not delete profile`() {
        val error = deleteNotExistingUser()

        assertThat(error.message).isEqualTo("Entity Not Found!")
    }

    @Test
    fun `should not become author profile`() {
        val error = becomeAuthorNotExitingProfile()

        assertThat(error.message).isEqualTo("Entity Not Found!")
    }

    @Test
    fun `should not update profile`() {
        val error = updateNotExistingProfile()

        assertThat(error.message).isEqualTo("Entity Not Found!")
    }
}
