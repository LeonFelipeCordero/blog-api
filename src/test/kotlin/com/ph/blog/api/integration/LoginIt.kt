package com.ph.blog.api.integration

import com.ph.blog.api.IntegrationTestBase
import com.ph.blog.api.auth.LoginAttemptService
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.util.stream.IntStream

class LoginIt : IntegrationTestBase() {

    @Autowired
    private lateinit var loginAttemptService: LoginAttemptService

    @Test
    fun `should log in`() {
        val profile = createProfile()

        val basicProfile = login()

        assertThat(profile.id!!).isEqualTo(basicProfile.id)

        deleteProfile(profile.id!!)
        loginAttemptService.approvedLogin(profile.email!!)
    }

    @Test
    fun `should not log in`() {
        val profile = createProfile()

        val response = failLogin()

        assertThat(response.message).isEqualTo("User Or Password Wrong")

        deleteProfile(profile.id!!)
        loginAttemptService.approvedLogin(profile.email!!)
    }

    @Test
    fun `should block user`() {
        val profile = createProfile()

        IntStream.range(0, 11).forEach { failLogin() }
        val response = blockLogin()

        assertThat(response.message).isEqualTo("user is blocked by to many attempts")

        deleteProfile(profile.id!!)
        loginAttemptService.approvedLogin(profile.email!!)
    }
}
