package com.ph.blog.api.integration

import com.ph.blog.api.IntegrationTestBase
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDate

class ArticleIT : IntegrationTestBase() {

    companion object {
        private const val title = "test title"
    }

    @Test
    fun `should create article`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        assertThat(article.id!!).isNotNull()
        assertThat(article.blogHtml).isEqualTo("<p>test</p>")
        assertThat(article.title).isEqualTo(title)
        assertThat(article.shortDescription).isEqualTo("test description")
        assertThat(article.postDate).isEqualTo(LocalDate.now())
        assertThat(article.author).isEqualTo(profile.id!!)
        assertThat(article.comments).isEmpty()

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }

    @Test
    fun `should find article`() {
        val profile = createProfile()
        val article = createArticle("test title", profile.id!!)

        val foundArticle = findArticle(article.id!!)

        assertThat(article.id!!).isEqualTo(foundArticle.id)
        assertThat(article.blogHtml).isEqualTo(foundArticle.blogHtml)
        assertThat(article.title).isEqualTo(foundArticle.title)
        assertThat(article.shortDescription).isEqualTo(foundArticle.shortDescription)
        assertThat(article.postDate).isEqualTo(foundArticle.postDate)
        assertThat(article.author).isEqualTo(foundArticle.author)
        assertThat(foundArticle.comments).isEmpty()

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }

    @Test
    fun `should update article`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        val updatedArticle = updateArticle(article.id!!)

        assertThat(updatedArticle.blogHtml).isEqualTo("second content")
        assertThat(updatedArticle.title).isEqualTo("second title")
        assertThat(updatedArticle.shortDescription).isEqualTo("second description")
        assertThat(updatedArticle.category).isEqualTo("another one")

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }

    @Test
    fun `should add comment to article`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        val response = addComment(article.id!!)
        val foundArticle = findArticle(article.id!!)

        assertThat(response.message).isEqualTo("Comment Saved")
        assertThat(foundArticle.comments).hasSize(1)
        assertThat(foundArticle.comments[0].content).isEqualTo("content")

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }

    @Test
    fun `should add comment to article being logged`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        val response = addCommentLogged(article.id!!, profile.id!!)
        val foundArticle = findArticle(article.id!!)

        assertThat(response.message).isEqualTo("Comment Saved")
        assertThat(foundArticle.comments).hasSize(1)
        assertThat(foundArticle.comments[0].content).isEqualTo("content")
        assertThat(foundArticle.comments[0].profile).isEqualTo(profile.id!!)

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }

    @Test
    fun `should delete article`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        val response = deleteArticle(article.id!!)

        assertThat(response.message).isEqualTo("Article deleted")

        deleteProfile(profile.id!!)
    }

    @Test
    fun `should find article by title`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        val articles = findArticleBy("title", title)

        assertThat(articles).hasSize(1)
        assertThat(articles[0].id!!).isEqualTo(article.id!!)

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }

    @Test
    fun `should find article by author`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        val articles = findArticleBy("author", profile.id!!)

        assertThat(articles).hasSize(1)
        assertThat(articles[0].id!!).isEqualTo(article.id!!)

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }

    @Test
    fun `should find article by category`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        val articles = findArticleBy("category", article.category!!)

        assertThat(articles).hasSize(1)
        assertThat(articles[0].id!!).isEqualTo(article.id!!)

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }

    @Test
    fun `should find article by date`() {
        val profile = createProfile()
        val article = createArticle(title, profile.id!!)

        val articles = findArticleBy("date", LocalDate.now().minusDays(1).toString())

        assertThat(articles).hasSize(1)
        assertThat(articles[0].id!!).isEqualTo(article.id!!)

        deleteProfile(profile.id!!)
        deleteArticle(article.id!!)
    }
}
